# Structure du projet
1 - src/client : code pour les clients     
2 - src/common : code commun au clients et serveur  
3 - src/server : code serveur    
3 - src/test   : code des tests    

# Build
```bash
$ make all
```
Suffit pour compiler tous les tests ainsi que la librairie (lib_psar.a). Le header pour celle ci est dans src/api/psar_dsm.h

# Documentation
## manager.c - le gestionnaire de requetes   
Les clients et le serveur raisonnent avec des pages. Quand un client 
demande acces a une plage memoire, il demande acces aux pages qui constituent cet espace memoire.
Le serveur doit donc gerer les requetes de ces pages / ressources.
Quand le manager recoit une requete lock, cette requete indique l'indice de la premiere
page demandee ainsi que le nombre de pages. Le manager parcout les ressources
correspondantes et si celle ci est libre, la 'donne' au client et incremente son compteur 'fulfilled', sinon, la met dans une file d'attente. Si le compteur 'fulfilled' de la 
requete est egale au nombre de ressources/pages demandees, la requete peut etre servie avec la fonction callout.
Lorsque le manager recoit une requete free:   
- si la premiere requete en attente est un write, on incremente le compteur 'fulfilled' de cette 
requete, et si fulfilled = le nombre de pages demandees, on appel callout     
- sinon, tant que les requetes en attente son des read, on leur donne acces a la ressource avec la 
meme logique de 'fulfilled' et nb pages demandees.

## client_manager.c - le gestionnaire de clients
La fonction callout du manager prend en parametre un pointeur vers une structure request comprenant l'identifiant du client. Afin de pouvoir repondre au client, il faut alors pouvoir retrouver sa socket associe. Le client_manager definit une table de hachage associant identifiant de client et une struct client comprenant la socket et la derniere requete de verouillage.

## server_api.c - implementation de l'api serveur
       
             
# Tests
## manager_test     
### Variables
1 - buf : tampon d'entiers de tailled BUFSZ    
2 - nFils : le nb de threads qui vont acceder le tampon en conccurence    
3 - iters : le nombre d'iterations de chaque thread     

### Principe
1 - Test 1     
Tous les threads sont des ecrivains qui reservent 2 cases du tampon
et incrementent les valeurs de 1. Si le manager gere bien l'exclusion mutuelle,
on aura buf[0] et buf[BUFSZ-1] = nFils * iters, et buf[i] = 2 * nfils * iters
pour les autres cases

2 - Test 2   
Les threads alternent entre les modes ecrivains et lecteur.     
On compte le nombre d'ecrivains et lecteurs dans chaque case 
periodiquement dans un autre thread (observer_thread).
On s'assure que le manager gere bien l'exclusion d'ecrivains et lecteurs
en comparant nReaders[i] et nWriters[i] pour 0 <= i <= BUFSZ

## client_manager_test
Test simple pour voir si le gestionnaire fait bien les insertions et supressions.

## batch_read_test
Test pour voir si la liberation d'une ressource verouillee par plusieurs clients est
correcte.

## client_test
Test de la tolerance aux pannes clients. On construit un file d'attente dans le gestionnaire de requetes avec 5 clients tq:    
client0 -> client1/client2 ->client3 -> client 4    
Les clients 0 et 3 quittent l'application sans liberer leur ressources. Les test est valide si tous les clients finissent par avoir acces a l'application et que le buffer lu par le client 4 a la fin est rempli de 3.

## mem_manager_test
Test le gestionnaire de requetes du cote client.