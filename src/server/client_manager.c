#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>

#include "../common/list.h"
#include "client_manager.h"

/*
Une simple table de hachage, sans mise a jour
de sa taille pour stocker des clients et leurs
sockets en leur associant un identifiant
*/

static int cpt = 0;

struct client_store_st{
    int size;
    int capacity;
    struct list_head buckets[INITIAL_CAPACITY];
}client_store;


int hash(int id){
    return id % client_store.capacity;
}

void init_client_manager(){
    client_store = (struct client_store_st){0, INITIAL_CAPACITY};
    for(int i = 0; i < INITIAL_CAPACITY; i++)
        INIT_LIST_HEAD(&client_store.buckets[i]);
}

/*
Prend en parametre une socket d'un client
et retourne une structure pour gerer le client
apres l'avoir ajoute a la table de hachage 
*/
client_t *add_client(int socketFD){
    client_t *client = malloc(sizeof(client_t));
    if(client == NULL){
        perror("client_manager.add_client malloc");
        return NULL;
    } 

    client->id = cpt++;
    client->sockfd = socketFD;
    INIT_LIST_HEAD(&client->lst);

    int idx = hash(client->id);
    list_add_tail(&client->lst, &client_store.buckets[idx]);
    client_store.size++;

    return client;
}

client_t *get_client(int id){
    int idx = hash(id);
    client_t *tmp;
    list_for_each_entry(tmp, &client_store.buckets[idx], lst){
        if(tmp->id == id)
            return tmp;
    }
    return NULL;
}

/*
Retire le client d'id id de la table de hachage
sans desallouer la structure associee
*/
void delete_client(int id){
    client_t *client = get_client(id);
    if(client == NULL)
        return;
    list_del(&client->lst);
}
