#ifndef __MANAGER_H__
#define __MANAGER_H__

#include "../common/list.h"
#include "../common/common.h"

typedef struct request_list{
    struct request *req;
    struct list_head list;
}request_list;


typedef struct ressource{
    int locked;
    int mode;
    int nReaders;
    int waiting;
    int owner;
    struct list_head queue;
}ressource;


void init_ressources(int n, void (*func)(request *func));

void ask_ressources(request *req);
void free_ressources(request *req);
void delete_request(request *req);
#endif // __MANAGER_H__