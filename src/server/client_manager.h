#ifndef __CLIENT_MANAGER_H__
#define __CLIENT_MANAGER_H__

#include <semaphore.h>
#include "../common/list.h"
#include "../common/common.h"

#define INITIAL_CAPACITY 50

typedef struct client{
    int id;
    int sockfd;
    //sem_t sem;
    request *last_lock;
    struct list_head lst;
}client_t;

void init_client_manager();
client_t *add_client(int socketFD);
client_t *get_client(int id);
void delete_client(int client_id);

#endif //__CLIENT_MANAGER_H__