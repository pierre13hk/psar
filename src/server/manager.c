#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include "manager.h"

static ressource *ressources = NULL;
static void (*callout)(request *req);
static int NRSR;
static pthread_mutex_t lock;

void init_ressources(int n, void (*func)(request *func)){
    NRSR = n;
    ressources = malloc(sizeof(struct ressource) * NRSR);
    if(ressources == NULL){
        perror("ressources malloc");
        return;
    }
    
    for(int i = 0; i < NRSR; i++){
        ressources[i].locked = 0;
        ressources[i].mode = 0;
        ressources[i].nReaders = 0;
        ressources[i].waiting = 0;
        ressources[i].owner = -1;
        INIT_LIST_HEAD(&ressources[i].queue);
    }

    pthread_mutex_init(&lock, NULL);
    callout = func;
}


int ask_ressource(ressource *rsrc, request *req){
    if(rsrc->locked){
        // a faire : si la demande est un read, 
        // et que la ressource est verouille en read
        // et que la queue est vide, donner acces
        request_list *rl = malloc(sizeof(request_list));
        if(rl == NULL){
            perror("ask ressource malloc");
            return 0;
        }
        rl->req = req;
        rsrc->waiting++;
        INIT_LIST_HEAD(&rl->list);
        list_add_tail(&rl->list, &(rsrc->queue));
        return 0;
    }else{
        rsrc->locked = 1;
        rsrc->mode = req->mode;
        if(req->mode == READ)
            rsrc->nReaders++;
        rsrc->owner = req->id;
        req->fullfilled++;
        return 1;
    }
}

void ask_ressources(request *req){
    pthread_mutex_lock(&lock);
    for(int i = 0; i < req->nb; i++){
        ressource *rsrc = &ressources[req->begin + i];
        ask_ressource(rsrc, req);
    }
    if(req->fullfilled == req->nb)
        callout(req);

    pthread_mutex_unlock(&lock);
}

void free_ressource(ressource *rsrc){
    if(rsrc->mode == READ){
        rsrc->nReaders--;
        if(rsrc->nReaders > 0)
            return;
    }
    if(list_empty(&rsrc->queue)){
        rsrc->locked = 0;
        rsrc->owner = -1;

    }else{
        request_list *top = list_first_entry(&rsrc->queue, request_list, list);
        request *next = top->req;
        rsrc->mode = next->mode;
        rsrc->owner = next->id;
        if(next->mode == WRITE){
            list_del(&top->list);
            next->fullfilled++;
            if(next->fullfilled == next->nb){
                callout(next);
            }
            free(top);
            return;
        }
        // donner acces a aux lecteurs qui attendent et s'arreter lorsqu'on
        // tombe sur la premiere requete write. strategie pour eviter une famine
        while(!list_empty(&rsrc->queue) && next->mode == READ  ){
            list_del(&top->list);
            next->fullfilled++;
            rsrc->nReaders++;
            if(next->fullfilled == next->nb)
                callout(next);

            free(top);
            top = list_first_entry(&rsrc->queue, request_list, list);
            next = top->req;

        }
        
    }
}

void free_ressources(request *req){
    pthread_mutex_lock(&lock);
    for(int i = 0; i < req->nb; i++){
        ressource *rsrc = &ressources[req->begin + i];
        free_ressource(rsrc);
    }
    pthread_mutex_unlock(&lock);
}

void delete_request(request *req){
    pthread_mutex_lock(&lock);
    for(int i = 0; i < req->nb; i++){
        ressource *rsrc = &ressources[req->begin + i];
        if(rsrc->owner == req->id){
            free_ressource(rsrc);
        }else{
            request_list *cur;
            int found = 0;
            list_for_each_entry(cur, &rsrc->queue, list){
                if(cur->req == req){
                    found = 0;
                    list_del(&cur->list);
                    break;
                }
            }
            // si on a rien trouve dans la liste et qu'on est pas owner de la rsrc
            // et qu'on est en mode read, on peut supposer etre un des owners
            if(!found && req->mode == READ && rsrc->mode == READ)
                free_ressource(rsrc);
        }
    }

    pthread_mutex_unlock(&lock);
}