#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <resolv.h>
#include <netinet/ip.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "server_api.h"
#include "manager.h"
#include "client_manager.h"
#include "../common/common.h"

#define PORT 1024

static int page_size = 1;
static int sockServer = -1;

//buffer mémoire coté serveur
static int dsm_size;
static char *dsm;

/*
* La methode qu'on utilise comme callout dans le manager.c
*/
void send_resp(request *req){
    req->mode |= OK;
    client_t *client = get_client(req->id);
    write(client->sockfd, req, sizeof(request));
    return;
}

void *client_handler(void *cl){
    client_t *client = (client_t*) cl;
    int requested_rsrc = 0;
    int ok = write(client->sockfd, &dsm_size, sizeof(int));
    while(1){
        request *r = malloc(sizeof(request));
        int succ = read(client->sockfd, r, sizeof(request));
        if(succ != sizeof(request)){
            int error = 0;
            socklen_t len = sizeof (error);
            int retval = getsockopt (client->sockfd, SOL_SOCKET, SO_ERROR, &error, &len);
            if(error != 0)
                fprintf(stderr, "socket error: %s\n", strerror(error));
            //printf("read %d bytes\n", succ);
            //perror("client_handler.read()");
            break;
        }

        if(r->mode & SYNCDOWN){
            char *adr = dsm + r->begin * page_size;
            write(client->sockfd, adr, page_size);
            free(r);
            continue;
        }

        if(r->mode & SYNCUP){
            char *adr = dsm + r->begin * page_size;
            int rd = read(client->sockfd, adr, page_size);
            while(rd < page_size)
                rd += read(client->sockfd, adr + rd, page_size - rd);
            free(r);
            continue;
        }

        r->id = client->id;
        r->fullfilled = 0;

        if(r->mode & RELSE){
            r->mode &= ~(RELSE);
            free_ressources(r);
            free(client->last_lock);
            client->last_lock = NULL;
        }else{
            client->last_lock = r;
            ask_ressources(r);
        }
        
    }
    close(client->sockfd);
    // Le client s'est deconnecte sans liberer des ressources
    // donc on les liberer pour pas que l'application soit bloquee
    if(client->last_lock != NULL){
        printf("Client %d exited without freeing ressources, freeing now\n", client->id);
        delete_request(client->last_lock);
        free(client->last_lock);
    }
    delete_client(client->id);
    free(client);
}

void *_InitMaster(int size){
    page_size = getpagesize();
    int fd;
    int flags = MAP_PRIVATE| MAP_FILE;
    size = (size / page_size) * page_size + page_size;
    dsm = (char*)malloc(size);
    dsm_size = size;

   
    if ((fd = open("/dev/zero", O_RDWR)) == -1) {
        perror("open");
        exit(1);
    }

    if (mmap(&dsm, dsm_size, PROT_READ|PROT_WRITE, flags, fd, 0) == (caddr_t)-1L) {
        perror("mmap");
        exit(1);
    }

    struct sockaddr_in server_info;
    memset(&server_info, 0, sizeof(server_info));
    server_info.sin_port = htons(PORT);
    server_info.sin_family = AF_INET;
    server_info.sin_addr.s_addr = INADDR_ANY;

    if((sockServer = socket(AF_INET, SOCK_STREAM, 0)) == -1){
        perror("Serveur -- Erreur création socket");
        exit(0);
    }

    puts("Serveur -- Socket setup !");

    int enable = 1;
    if (setsockopt(sockServer, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        perror("setsockopt(SO_REUSEADDR) failed");

    if((bind(sockServer, (struct sockaddr*) &server_info, sizeof(server_info)))==-1){
        perror("Serveur -- Bind fail");
        exit(0);
    }
                                

    puts("Serveur -- Server bind !");

    if((listen(sockServer, 10))==-1){
        perror("Serveur -- Listen fail");
        exit(0);
    }

    puts("Serveur -- Server is listenning...");


    // allouer memoire et manager
    int npages = dsm_size / page_size; // a changer
    // table de hachage pour gerer les clients
    init_client_manager();
    // gestionnaire de requetes / file
    init_ressources(npages, send_resp);
}

void _LoopMaster(){
    // A voir si on garde ce modele de 1 client = 1 thread
    while(1){
        struct sockaddr_in exp;
		socklen_t len = sizeof(exp);
		int clientfd = accept(sockServer, (struct sockaddr*) & exp, &len);
		if(clientfd < 0){
			perror("SeverSocket::ServerSocket error : accept()");
            continue;
		}

        // est-ce qu'on devrait conserver les infos thread?
        client_t *client = add_client(clientfd);
        pthread_t th;
        pthread_create(&th, NULL, &client_handler, client);
    }
}