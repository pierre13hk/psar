#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

#include "../api/psar_dsm.h"

#define BUFSZ 100
#define NFILS 5
#define iters 5

static int cpt = 0;
void client(){
    sleep(1);

    char* dsm = InitSlave("127.0.0.1");
    if (cpt == 0) {
        lock_write(dsm,  BUFSZ);
        for(int j = 0; j < BUFSZ; j++)
            dsm[j] = dsm[j] + 1;
        
        unlock_write(dsm, BUFSZ);
        puts("\nwriter proc wrote to buf");

        sleep(1);

        lock_write(dsm,  BUFSZ);
        for(int j = 0; j < BUFSZ; j++)
            printf("%d|", dsm[j]);
        
        unlock_write(dsm, BUFSZ);
        puts("\nwriter proc read from buf");
        exit(0);
    }

    sleep(1);
    lock_read(dsm, BUFSZ);
    for(int j = 0; j < BUFSZ; j++)
            printf("%d|", dsm[j]);
    unlock_read(dsm, BUFSZ);
    puts("\nreader proc exited");
    exit(0);
}

int main(int argc, char **argv){
    pid_t serv_pid = 0;
    if ((serv_pid = fork()) == 0) {
        InitMaster(BUFSZ);
        LoopMaster();
    }
    for(int i = 0; i < NFILS; i++){
        cpt = i;
        if(fork() == 0)
            client();
    }
    
    for(int i = 0; i < NFILS; i++)
        wait(NULL);
    
    
    kill(serv_pid, SIGKILL);
    
    exit(0);
}