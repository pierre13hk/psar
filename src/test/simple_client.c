#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include "../api/psar_dsm.h"

#define BUFSZ 100
#define iters 10

int main(int argc, char **argv){
    if(argc< 2){
        puts("pas assez d'arguments, donner une addresse ip");
        return 0;
    }
    sleep(1);

    int* dsm = (int*)InitSlave(argv[1]);
    for(int i = 0; i < iters; i++){
        lock_write(dsm,  BUFSZ);
        dsm[0]++;
        sleep(1);
        printf("Cpt = %d\n", dsm[0]);
        unlock_write(dsm, BUFSZ);
    }

    exit(0);
}
