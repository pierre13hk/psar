#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

#include "../api/psar_dsm.h"

#define BUFSZ 100
#define NFILS 5
#define iters 5

static int cpt = 0;
/*
* On veut creer un file de client dans le manager de :
* client 0 -> client 1 / client 2 -> client 3 -> client 4
* Les clients 0 et 3 quittent l'application sans liberer
* leurs ressourses.
* Notre systeme est tolerant a ces pannes si les clients 1, 2, et 4
* on acces a la section critique.
*/
void client(){
    char* dsm = InitSlave("127.0.0.1");
    
    switch(cpt){
    case (0):
        lock_write(dsm, BUFSZ);
        sleep(1);
        break;
    case (1):
    case (2):
        sleep(1);
        lock_write(dsm, BUFSZ);
        for(int j = 0; j < BUFSZ; j++)
            dsm[j] = dsm[j] + 1;
        sleep(2);
        unlock_write(dsm, BUFSZ);
        break;
    case (3):
        sleep(2);
        lock_write(dsm, BUFSZ);
        sleep(1);
        break;
    case (4):
        sleep(3);
        lock_write(dsm, BUFSZ);
        for(int j = 0; j < BUFSZ; j++)
            dsm[j] = dsm[j] + 1;
        sleep(2);
        unlock_write(dsm, BUFSZ);
        // verifier l'etat du buffer

        lock_read(dsm, BUFSZ);
        for(int j = 0; j < BUFSZ; j++)
                printf("%d|", dsm[j]);
        unlock_read(dsm, BUFSZ);
    }

    printf("\nChild proc %d exited\n", cpt);
    exit(0);
}

int main(int argc, char **argv){
    pid_t serv_pid = 0;
    if ((serv_pid = fork()) == 0) {
        InitMaster(BUFSZ);
        LoopMaster();
    }
    for(int i = 0; i < NFILS; i++){
        cpt = i;
        if(fork() == 0)
            client();
    }
    
    for(int i = 0; i < NFILS; i++)
        wait(NULL);
    
    
    kill(serv_pid, SIGKILL);
    
    exit(0);
}