#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>


#include "../api/psar_dsm.h"

int main(int argc, char **argv){
    int sz = 0;
    if(argc > 1)
        sz = atoi(argv[1]);
    if(!sz)
        sz = 1024;

    printf("%s %d Sz = %d\n", argv[1], argc,  sz);
    InitMaster(sz);
    LoopMaster();
    
}