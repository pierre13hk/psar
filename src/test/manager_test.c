#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>

#include "../common/common.h"
#include "../server/manager.h"

#define BUFSZ 10
#define nFils 20
#define iters 100

// descripteur de fichiers pour communication serveur->threads
int childFDS[nFils][2];
// le tampon en mutex
int buf[BUFSZ];

// la frequence a laquelle on va verifier l'etat de nReaders et nWriters
#define checkFreq 5
// le nombre de lecteurs/ecrivains dans chaque case
int nReaders[BUFSZ];
int nWriters[BUFSZ];
int finished = 0;
int checkTimer = 0;
static pthread_mutex_t readersLocks[BUFSZ];
static pthread_mutex_t writersLocks[BUFSZ];
static sem_t checkTimerLock;
//static pthread_cond_t checkTimeCond;

typedef struct targs{
    int id;
    int writeFD;
    int readFD;
}thread_args;

int contains(int tab[], int n, int v){
    for(int i = 0; i < n; i++){
        if(tab[i] == v)
            return 1;
    }
    return 0;
}

int* random_order(int size){
    int *tab = malloc(size * sizeof(int));
    for(int i = 0; i < size; i++)
        tab[i] = -1;

    for(int i = 0; i < size; i++){
        int v = rand() % size;
        while(contains(tab, size, v))
            v = rand() % size;
        tab[i] = v;
    }

    return tab;
}


void* writer_thread(void *args){
    thread_args *a = (thread_args *)args;
    int id = a->id, writeFD = a->writeFD, readFD = a->readFD;
    free(args);
    for(int j = 0; j < iters; j++){
        int *order = random_order(BUFSZ - 1);
        for(int i = 0; i < BUFSZ - 1; i++){
            int idx = order[i];
            request *r = new_request(id, WRITE, idx, 2);
            write(writeFD, r, sizeof(request));

            char tmp[64];
            int res = read(readFD, tmp, sizeof(tmp));
            
            //on a eu la reponse/permission
            buf[idx]   = buf[idx]+1;
            buf[idx+1] = buf[idx+1] + 1;
            //sleep(1);
            r->mode = WRITE | RELSE;

            write(writeFD, r, sizeof(request));
            free(r);
        }
        free(order);
    }
}

void serveur(int readFD){
    //int readFD = perereadFD;
    for(int i = 0; i < nFils * iters * (BUFSZ - 1) * 2; i++){
        request *r = malloc(sizeof(request));
        int succ = read(readFD, r, sizeof(request));
        if(succ < 0)
            perror("Pere.read()");
        if(r->mode & RELSE){
            r->mode &= ~(RELSE);
            free_ressources(r);
            free(r);
        }else
            ask_ressources(r);
    }
}

void repond_fils(request *req){
    int ok = 1;
    write(childFDS[req->id][1], &ok, sizeof(int));
    free(req);
}

static int test_writer_mutex_ok = 0;
void test_writer_mutex(){
    init_ressources(BUFSZ, repond_fils);
//    init_callout(repond_fils);
    for(int i = 0; i < BUFSZ; i++)
        buf[i] = 0;
    pthread_t ids[nFils];
    int fd[2];
    pipe(fd);
    for(int i = 0; i < nFils; i++){
        pipe(childFDS[i]);
        thread_args *a = malloc(sizeof(thread_args));
        a->id = i;
        a->writeFD = fd[1];
        a->readFD = childFDS[i][0];
        pthread_create(&ids[i], NULL, &writer_thread, a);
        
    }
    serveur(fd[0]);
    
    int ok = 1;
    int passovers = nFils * iters;
    ok = ok && (buf[0] == passovers);
    ok = ok && (buf[BUFSZ - 1] == passovers);
    for(int i = 1; i < BUFSZ - 1; i++)
        ok = ok && (buf[i] == 2 * passovers);
    
    test_writer_mutex_ok = ok;
}

void* reader_writer_thread(void *args){
    thread_args *a = (thread_args *)args;
    int id = a->id, writeFD = a->writeFD, readFD = a->readFD;
    free(args);
    for(int j = 0; j < iters; j++){
        int *order = random_order(BUFSZ - 1);
        int isWriter = j % 2;
        //printf("is writer %d\n",  isWriter);
        for(int i = 0; i < BUFSZ - 1; i++){
            if(isWriter){
                int idx = order[i];
                request *r = new_request(id, WRITE, idx, 2);
                write(writeFD, r, sizeof(request));

                char tmp[64];
                int res = read(readFD, tmp, sizeof(tmp));
                
                //on a eu la reponse/permission
                //printf("\t\t|%d got permission for %d|\n", id, idx);
                buf[idx]   = buf[idx]+1;
                buf[idx+1] = buf[idx+1] + 1;

                nWriters[idx]++;
                nWriters[idx+1]++;

                nWriters[idx]--;
                nWriters[idx+1]--;
                
                r->mode = WRITE | RELSE;

                write(writeFD, r, sizeof(request));
                free(r);
            }else{
                int idx = order[i];
                request *r = new_request(id, READ, idx, 2);
                write(writeFD, r, sizeof(request));

                char tmp[64];
                int res = read(readFD, tmp, sizeof(tmp));
                
                //on a eu la reponse/permission
                //printf("\t\t|%d got permission for %d|\n", id, idx);
                //puts("got read");
                pthread_mutex_lock(&readersLocks[idx]);
                pthread_mutex_lock(&readersLocks[idx + 1]);
                nReaders[idx]++;
                nReaders[idx+1]++;
                pthread_mutex_unlock(&readersLocks[idx]);
                pthread_mutex_unlock(&readersLocks[idx + 1]);
                

                r->mode = READ | RELSE;

                write(writeFD, r, sizeof(request));
                //puts("wrote release");
                pthread_mutex_lock(&readersLocks[idx]);
                pthread_mutex_lock(&readersLocks[idx + 1]);
                nReaders[idx]--;
                nReaders[idx+1]--;
                pthread_mutex_unlock(&readersLocks[idx]);
                pthread_mutex_unlock(&readersLocks[idx + 1]);
                free(r);
            }
            if(++checkTimer % checkFreq == 0){
                sem_post(&checkTimerLock);
            }
        }
        free(order);
    }
    return NULL;
}

static int observer_thread_ok = 0;
/*
Regarde nReaders et nWriters a une frequence checkFreq
pour s'assurer qu'on a pas d'ecrivains et lecteurs en meme temps
*/
void* observer_thread(void* args){
    int ok = 1;
    while(!finished){
        sem_wait(&checkTimerLock);
        for(int i = 0; i < BUFSZ; i++){
            pthread_mutex_lock(&readersLocks[i]);
            if(nReaders[i]){
                if(nWriters[i]){
                    printf("\t\t\tfailed mutex : nReaders[%d] = %d, nWriters[%d] = %d\n", i, nReaders[i], i, nWriters[i]);
                    ok = 0;
                }

            }
            pthread_mutex_unlock(&readersLocks[i]);
        }
    }
    observer_thread_ok = ok;
}

void test_reader_writer_mutex(){
    init_ressources(BUFSZ,repond_fils);
    //init_callout(repond_fils);
    int mtx = 0;
    for(int i = 0; i < BUFSZ; i++){
        buf[i] = 0;
        mtx = mtx || pthread_mutex_init(&readersLocks[i], NULL);
        mtx = mtx || pthread_mutex_init(&writersLocks[i], NULL);
    }
    if(mtx){
        perror("mutex init failed");
        return;
    }
    // initialiser le thread observateur
    sem_init(&checkTimerLock, 0, 1);
    sem_wait(&checkTimerLock);
    pthread_t observer;
    pthread_create(&observer, NULL, &observer_thread, NULL);

    pthread_t ids[nFils];
    int fd[2];
    pipe(fd);
    for(int i = 0; i < nFils; i++){
        pipe(childFDS[i]);
        thread_args *a = malloc(sizeof(thread_args));
        a->id = i;
        a->writeFD = fd[1];
        a->readFD = childFDS[i][0];
        pthread_create(&ids[i], NULL, &reader_writer_thread, a);
        
    }

    serveur(fd[0]);

    finished = 1;
    sem_post(&checkTimerLock);

}



int main(int argc, char **argv){
    srand(time(NULL));  

    test_writer_mutex();
    test_reader_writer_mutex();

    if(test_writer_mutex_ok)
        puts("test_writer_mutex : PASSED");
    else
        puts("test_writer_mutex : FAILED");

    if(observer_thread_ok)
        puts("test_reader_writer_mutex : PASSED");
    else
        puts("test_reader_writer_mutex : FAILED");

}