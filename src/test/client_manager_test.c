#include <stdio.h>
#include <stdlib.h>

#include "../server/client_manager.h"

int main(int argc, char **argv){
    init_client_manager();
    client_t *id1 = add_client(0);
    client_t *id2 = add_client(1);

    printf("Client 1 id = %d, client 2 id = %d\n", id1->id, id2->id);

    client_t *tmp = get_client(id1->id);
    if(tmp != id1) puts("get cl1 error");
    else           puts("get cl1 ok");
    tmp = get_client(id2->id);
    if(tmp != id2) puts("get cl2 error");
    else           puts("get cl2 ok");
    
    delete_client(id1->id);
    tmp = get_client(id1->id);
    if(tmp != NULL) puts("delete cl1 error");
    else            puts("delete cl1 ok");
    tmp = get_client(id2->id);
    if(tmp != id2) puts("get cl2 error");
    else           puts("get cl2 ok");

    free(id1);
    free(id2);
    return 0;
}