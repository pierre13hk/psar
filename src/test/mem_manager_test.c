#include <stdio.h>

#include "../client/mem_manager.h"

#define assert(have, want) \
    if(have != want)       \
        puts("error");     \
    else                   \
        puts("ok")         \


int main(int argc, char **argv){
    init_mem_manager(3);
    int a1 = ask_page(1);
    int a2 = ask_page(2);

    int f1 = free_page(1);
    int f2 = free_page(2);
    int f3 = free_page(0);
    
    assert(a1, 1);
    assert(a2, 1);
    assert(f1, 0);
    assert(f2, 0);
    assert(f3, 0);

    free_mem_manager();
}