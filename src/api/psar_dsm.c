#include "psar_dsm.h"
#include "../client/client_api.h"
#include "../server/server_api.h"


void *InitMaster(int size){
    return _InitMaster(size);
}

void LoopMaster(){
    _LoopMaster();
}

void *InitSlave(char *HostMaster){
    return _InitSlave(HostMaster);
}

void lock_read(void *adr, int s){
    _lock_read(adr, s);
}

void unlock_read(void *adr, int s){
    _unlock_read(adr, s);
}

void lock_write(void *adr, int s){
    _lock_write(adr, s);
}

void unlock_write(void *adr, int s){
    _unlock_write(adr, s);
}
