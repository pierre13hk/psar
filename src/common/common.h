#ifndef __COMMON_H__
#define __COMMON_H__

#define LOCK 0
#define UNLOCK 1

#define READ       0
#define WRITE      1
#define RELSE      2
#define EXIT       4
#define OK         8
#define SYNCDOWN  16
#define SYNCUP    32

#define PORT 1024

typedef struct request{
    int id;
    int mode;       // le type de la requete
    int begin;      // le numero de la premiere ressource demandee
    int nb;         // le nombre de ressources demandees
    int fullfilled; // le nombre de ressources obtenues / verouillees
}request;


request* new_request(int id, int mode, int begin, int n);
request* new_request_mem(int id, int mode, char *dsm, char *begin, int sz, int page_size);
int get_adr_page(char *dsm, char *adr, int page_size);
int get_num_rsrc(int memsz, int page_size);

#endif //__COMMON_H__