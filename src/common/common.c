#include <stdlib.h>
#include <stdio.h>

#include "common.h"

request* new_request(int id, int mode, int begin, int n){
    request *req = malloc(sizeof(request));
    if(req == NULL){
        perror("new_request malloc");
        return NULL;
    }

    req->id = id;
    req->mode = mode;
    req->begin = begin;
    req->nb = n;
    req->fullfilled = 0;
    return req;
}


request* new_request_mem(int id, int mode, char *dsm, char *begin, int sz, int page_size){
    request *req = malloc(sizeof(request));
    if(req == NULL){
        perror("new_request malloc");
        return NULL;
    }

    req->id = id;
    req->mode = mode;
    req->begin = get_adr_page(dsm, begin, page_size);
    req->nb = get_num_rsrc(sz, page_size);
    req->fullfilled = 0;
    return req;
}

int get_adr_page(char *dsm, char *adr, int page_size){
    return (adr - dsm) / page_size;
}


int get_num_rsrc(int memsz, int page_size){
    return memsz / page_size + 1;
}