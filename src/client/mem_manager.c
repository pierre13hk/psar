#include <stdlib.h>

#include "mem_manager.h"

static int n_pages = 0;
static page_t *pages = NULL;

int init_mem_manager(int npages){
    if(npages < 1)
        return 0;

    pages = malloc(sizeof(page_t) * npages);
    if(pages == NULL)
        return 0;

    n_pages = npages;
    for(int i = 0; i < n_pages; i++)
        pages[i].status = 0;
    
    return 1;
}

int ask_page(int page_idx, int mode){
    if(page_idx < 0 || page_idx > n_pages - 1)
        return 0;
    pages[page_idx].status = ASKED | mode;
    return pages[page_idx].status;
}

int free_page(int page_idx, int mode){
    if(page_idx < 0 || page_idx > n_pages - 1)
        return 0;
    if(!pages[page_idx].status & ASKED)
        return 0;

    pages[page_idx].status = pages[page_idx].status & ~ASKED & ~mode;
    return pages[page_idx].status;
}

int page_status(int page_idx){
    if(page_idx < 0 || page_idx > n_pages - 1)
        return -1;
    return pages[page_idx].status;
}

void free_mem_manager(){
    if(n_pages > 0)
        free(pages);
}