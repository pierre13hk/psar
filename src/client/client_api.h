#ifndef __CLIENT_API_H__
#define __CLIENT_API_H__

void *_InitSlave(char *HostMaster);
void _lock_read(void *adr, int s);
void _unlock_read(void *adr, int s);
void _lock_write(void *adr, int s);
void _unlock_write(void *adr, int s);

#endif //__CLIENT_API_H__