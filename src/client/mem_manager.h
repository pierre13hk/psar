#ifndef __MEM_MANAGER_H__
#define __MEM_MANAGER_H__

#define FREE     0
#define ASKED    1
#define MREAD    2
#define MWRITE   4
#define ACCESSED 8

typedef struct page{
    int status;
}page_t;

int init_mem_manager(int npages);
int ask_page(int page_idx, int mode);
int free_page(int page_idx, int mode);
int page_status(int page_idx);
void free_mem_manager();

#endif //__MEM_MANAGER_H__