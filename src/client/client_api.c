#define _GNU_SOURCE
#define PORT 1024

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <resolv.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <ucontext.h>
#include <fcntl.h>
#include <netinet/ip.h>
#include <netinet/in.h> 
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#include "client_api.h"
#include "mem_manager.h"
#include "../common/common.h"

static int id = 0;
static int page_size = 1;

static int sock = -1;
static char *dsm;

static void sync_page(int page_num){
    request r = {0, SYNCDOWN, page_num, 1, 0};
    write(sock, &r, sizeof(request));
    void *adr = dsm + page_num * page_size;
    int rd = read(sock, adr, page_size);
    while(rd < page_size)
        rd += read(sock, adr + rd, page_size - rd);
    
}

static int aligned_mprotect(char *adr, int len, int flags){
    /*
    * L'addresse passee a mprotect doit etre alignee sur une page. 
    */
    int err = 0;
    long pg_begin = (long)adr, tmp = page_size;
    pg_begin -= (pg_begin % page_size);
    err = mprotect((char*)pg_begin, adr - (char*)pg_begin + len, flags);
    return err;
}

static void tsigsegv(int sig, siginfo_t *si, void *context) {
    /* adresse de la page qui a fait le defaut alignée  */
    int page_num = get_adr_page(dsm, si->si_addr, page_size);
    int page_stat = page_status(page_num);
    if(!page_stat & ASKED){
        // on essaye d'acceder a une page que l'on a pas demandee
        printf("tsigsegv for a page that wasn;t asked, pgno : %d\n", page_num);
        return;
    }

    int err = aligned_mprotect(si->si_addr, 1, PROT_WRITE | PROT_READ);

    if(err){
        //a faire
        puts("tsigsegv aligned mprotect 1 fail");
        sleep(5);
    }
    ask_page(page_num, page_stat | ACCESSED);
    sync_page(page_num);
    if(page_stat & MREAD)
        aligned_mprotect(si->si_addr, 1, PROT_READ);
}


void *_InitSlave(char *HostMaster){
    caddr_t adr;
    int fd;
    int flags = MAP_PRIVATE| MAP_FILE;
    struct sigaction sa;
    page_size = getpagesize();

    /* definir le handler de SIGSEGV */
    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = tsigsegv;
    if (sigaction(SIGSEGV, &sa, NULL) == -1){
        perror("sigaction");
        exit(1);
    }

    page_size = getpagesize();

    /* initialiser le socket */
    sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(HostMaster);
    serv_addr.sin_port = htons(PORT);

    /* connexion avec server */
    if(connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
        perror("échec de connexion\n");
    }

    int dsm_size = 0;
    read(sock, &dsm_size, sizeof(int));

    // a faire : comparaison de la taille des pages du
    // serveur et la taille des pages en local pour voir si elles sont compatibles

    /* Initialiser le segment a 0 */
    if ((fd = open("/dev/zero", O_RDWR)) == -1) {
        perror("open /dev/zero");
        exit(1);
    }

    /* Aligner la taille sur un multiple de pages */
    dsm_size = (dsm_size / page_size) * page_size + page_size;

    int n_pages = dsm_size / page_size;
    init_mem_manager(n_pages);

    /* Demander la reservation du zone de memoire
       Associer les droits a cette zone R/W afin d'initialiser le segment partagé  
    */
    if ( (adr = mmap(0, dsm_size, PROT_READ|PROT_WRITE, flags, fd, 0)) == (caddr_t)-1L) {
        perror("mmap");
        exit(1);
    }

    //my_ressources = (ressource *) adr;

    // initialise les ressources à partir de l'adresse adr
    dsm = adr;
    /* invalider les droits d'accès */
    mprotect((caddr_t)dsm, dsm_size, PROT_NONE);

    return adr;
}

int mark_pages(void *adr, int s, int ask, int mode){
    if(adr < (void*)dsm || s < 0)
        return 0;
    int start = get_adr_page(dsm, adr, page_size);
    int n_pages = get_num_rsrc(s, page_size);
    for(int i = 0; i < n_pages; i++){
        if(ask){
            ask_page(start + i, mode);
        }else{
            free_page(start + i, 0);
        }
    }
}

void _lock_read(void *adr, int s){
    request *req = new_request_mem(id, READ, dsm, adr, s, page_size);
    if((write(sock, req, sizeof(request)) != sizeof(request))){
        perror("writing_lock_read fail");
        return;
    }

    if(read(sock, req, sizeof(request)) == -1){
        perror("Reading_lock_read fail");
        return;
    }
    int ok = mark_pages(adr, s, 1, MREAD);
}

void _unlock_read(void *adr, int s){
    int ok = mark_pages(adr, s, 0, MREAD);
    // a faire : verifier qu'on libere des pages qu'on a demande

    request *req = new_request_mem(id, READ | RELSE, dsm, adr, s, page_size);
    req->mode = READ | RELSE;
    if((write(sock, req, sizeof(request)) != sizeof(request))){
        perror("writing_unlock_read fail");
        exit(0);
    }
    /* invalider les droits d'accès quand on lache le verrou */
    aligned_mprotect((caddr_t)adr, s, PROT_NONE);
    free(req);
}

void _lock_write(void *adr, int s){
    request *req = new_request_mem(id, WRITE, dsm, adr, s, page_size);
    if((write(sock, req, sizeof(request)) != sizeof(request))){
        perror("writing_lock_write fail");
        return;
    }
    
    if(read(sock, req, sizeof(request)) != sizeof(request)){
        perror("Reading_lock_write fail");
        return;
    }
    int ok = mark_pages(adr, s, 1, MWRITE);
}

void _unlock_write(void *adr, int s){
    // a faire : verifier qu'on libere des pages qu'on a demande

    int start = get_adr_page(dsm, adr, page_size);
    int n_pages = get_num_rsrc(s, page_size);
    for(int i = 0; i < n_pages; i++){
        int stat = page_status(start + i);
        if(stat & ACCESSED){
            request r = {0, SYNCUP, start + i, 1, 0};
            write(sock, &r, sizeof(request));
            void *adr = dsm + (start + i) * page_size;
            write(sock, adr, page_size);
        }

    }

    int ok = mark_pages(adr, s, 0, MWRITE);

    request *req = new_request_mem(id, WRITE | RELSE, dsm, adr, s, page_size);
    req->mode = WRITE | RELSE;
    if((write(sock, req, sizeof(request)) != sizeof(request))){
        perror("writing_unlock_write fail");
        exit(0);
    }
    
    /* invalider les droits d'accès quand on lache le verrou */
    int err = aligned_mprotect((caddr_t)adr, s, PROT_NONE);
    if(err)
        perror("unlock_write mprotect");
    free(req);
}