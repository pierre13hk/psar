CC     = gcc
CFLAGS = -g -pthread


OUTDIR = bin
DIR_OBJ = obj

SUBDIR = client common server test
INCS = $(wildcard *.h $(foreach fd, $(SUBDIR), src/$(fd)/*.h))
SRCS = $(wildcard *.c $(foreach fd, $(SUBDIR), src/$(fd)/*.c))
OBJS = $(addprefix $(DIR_OBJ)/, $(SRCS:c=o)) # obj/xxx.o obj/folder/xxx .o
INC_DIRS = $(addprefix -I, $(SUBDIR))

$(DIR_OBJ)/%.o: %.c $(INCS)
	mkdir -p $(@D)
	mkdir -p $(OUTDIR)
	$(CC) -o $@ -c $< $(CFLAGS) $(INC_DIRS)

OBJSRC = obj/src

OBJSRCC     = $(OBJSRC)/client
OBJSRCS     = $(OBJSRC)/server
CLIENT_OBJS = $(OBJSRCC)/client_api.o $(OBJSRCC)/mem_manager.o
SERVER_OBJS = $(OBJSRCS)/client_manager.o $(OBJSRCS)/manager.o $(OBJSRCS)/server_api.o
COMMON_OBJS = $(OBJSRC)/common/common.o


manager_test: $(OBJSRC)/server/manager.o $(OBJSRC)/test/manager_test.o $(OBJSRC)/common/common.o
	$(CC) $(CFLAGS) -o $(OUTDIR)/$@ $^ 
	
server_test: $(OBJSRC)/test/server_test.o 
	$(CC) $(CFLAGS) -o $(OUTDIR)/$@ $^ -L ./$(OUTDIR) -l:lib_psar.a

client_manager_test: $(OBJSRC)/server/client_manager.o $(OBJSRC)/test/client_manager_test.o $(OBJSRC)/common/common.o
	$(CC) $(CFLAGS) -o $(OUTDIR)/$@ $^ 

client_test:$(OBJSRC)/test/client_test.o
	$(CC) $(CFLAGS) -o $(OUTDIR)/$@ $^ -L ./$(OUTDIR) -l:lib_psar.a

simple_client:$(OBJSRC)/test/simple_client.o
	$(CC) $(CFLAGS) -o $(OUTDIR)/$@ $^ -L ./$(OUTDIR) -l:lib_psar.a

batch_read_test:$(OBJSRC)/test/batch_read_test.o
	$(CC) $(CFLAGS) -o $(OUTDIR)/$@ $^ -L ./$(OUTDIR) -l:lib_psar.a

mem_manager_test: $(OBJSRC)/client/mem_manager.o $(OBJSRC)/test/mem_manager_test.o 
	$(CC) $(CFLAGS) -o $(OUTDIR)/$@ $^

psar_static_lib: $(OBJSRC)/api/psar_dsm.o $(CLIENT_OBJS) $(SERVER_OBJS) $(COMMON_OBJS)
	ar rcs $(OUTDIR)/lib_psar.a  $^

all: psar_static_lib manager_test server_test client_manager_test client_test simple_client batch_read_test


clean:
	rm -rf bin obj